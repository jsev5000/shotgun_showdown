Environment Variables
| var | description |
|----|---|
|api_key   |      OpenAI API key |
|leagueId       | ESPN Fantasy Football leagueId (find in ESPN fantasy url)|
|leagueYear     | Year the season starts|
|espnS2         | found in cookies on ESPN fantasy website|
|swid           | found in cookies on ESPN fantasy website|
|bot_token      | for slackbot|
|signing_secret | for slack slash commands| 
|debugFlag      | For espn-api |


References:

https://github.com/cwendt94/espn-api/tree/master

https://platform.openai.com/docs/api-reference/chat/create?lang=python


Notes:
