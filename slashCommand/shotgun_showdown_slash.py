import espn_api
import time, math, random
from espn_api.football import League 
import logging, schedule, os
from slashCommand import Slash
from flask import Flask, request, make_response, Response
from slack_sdk import WebClient
from slack_sdk.signature import SignatureVerifier
from slack_sdk.errors import SlackApiError

#########
api_key = os.environ['api_key']
leagueId = int(os.environ['leagueId'])
leagueYear = int(os.environ['leagueYear'])
espnS2= os.environ['espnS2']
swid= os.environ['swid']
bot_token = os.environ['bot_token']
signing_secret = os.environ['signing_secret']
debugFlag = eval(os.environ['debugFlag'])
#########

week1=1662609600.0+31556926
weekLength=604800
ownerAndTag ={
                "Devin Lew":"<@U7SEJ0SQ7>",
                "Ari Memar":"<@U7WRN7DEH>",
                "Jeff Hutchens":"<@U7RJGBABX>",
                "Scott Nelson":"<@U93EP17U5>",
                "Adam Wyatt":"<@U7MFL5HPS>",
                "Andrew Riehl":"<@U7VTW6P6V>",
                "Jason Meagher":"<@U7RDP9CEQ>",
                "Scott Morrison":"<@U7XGWE6R5>",
                "Jordan Severance":"<@U80FQ0FNH>",
                "Chris Guerrero":"<@U7R9RS6FM>",
                "Kevin Caldwell":"<@U7SC92SQ2>",
                "David Skara":"<@U7RC0R18A>"
            }
slack_channel = '#test'#dsp_fantasy_football'
#########
#########
app = Flask(__name__)
#########

def currentWeek():
    now = time.time()
    week = math.ceil((now-week1)/weekLength)
    return week

def endOfWeek():
    outWeek = ['Tuesday','Wednesday']
    now = time.time()
    day = time.strftime('%A',time.localtime(now))
    if day in outWeek: return True
    else: return False

def talkShit(teamOwner):
    msg = ''
    try:
        tag = ownerAndTag[teamOwner]
        shitTalk = [
            f'{tag}, you suck at fantasy. Go home, pack it up. Give Jeremy your team.',
            f'{tag}, you gotta get better bro. :trashy:',
            f'{tag}, your dignity called, seems like it is completely lost.',
            f"Someone check on {tag}, he can't be in a good head space with this performance.",
            f"If I was as bad at fantasy as {tag}... well, I guess there would be two incredibly bad teams.",
            f"Paging custodial. {tag}'s team just took a shit in aisle 3. :poop:",
            f"Dude. {tag}. You ok this week?",
            f"{tag}, I don't even want to talk shit here. Let's be real. That was trash. Maybe give up this hobby.",
            f"{tag} really wanted to win this week, but I guess he forgot to set his lineup. Otherwise this would be pretty sad. :sad:"
        ]
        msg += random.choice(shitTalk)+'\n'
    except:
        msg += (f'{teamOwner}, you are terrible at fantasy and even worse at life.\n')
    return msg

def shotgunCandidate(league,week):
    currentLowScore = 100000
    currentLowOwner = ''
    projectedLowScore = 100000
    projectedLowOwner = ''
    for each in league.box_scores(week=week):
        if each.away_score < currentLowScore:
            currentLowScore = each.away_score
            currentLowOwner = each.away_team.owner
        if each.home_score < currentLowScore:
            currentLowScore = each.home_score
            currentLowOwner = each.home_team.owner
        if each.away_projected < projectedLowScore:
            projectedLowScore = each.away_projected
            projectedLowOwner = each.away_team.owner 
        if each.home_projected < projectedLowScore:
            projectedLowScore = each.home_projected
            projectedLowOwner = each.home_team.owner 
    return currentLowOwner,currentLowScore,projectedLowOwner,projectedLowScore

def lostAgain(teamOwner,league,week):
    shotgunMultiplier = 1 
    lastWeek = week-1
    while True:
        lastGunner = shotgunCandidate(league,lastWeek)
        if lastWeek == 0:
            break
        elif lastGunner[0] == teamOwner:
            lastWeek -=1
            shotgunMultiplier +=1
        else:
            break
    return shotgunMultiplier

def endTheWeek(teamOwner,points,league,week):
    msg = ''
    howMany = lostAgain(teamOwner,league,week)
    if howMany > 1: sss = 's'
    else: sss=''
    msg += f'Hell of a week {week}, gentlemen. Unfortunately, week {week} was hell for {teamOwner}.\nEnding the week with a pathetic {points} points, {ownerAndTag[teamOwner]} you owe {howMany} shotgun{sss}.\n'
    return msg

def midWeek(gunner):
    msg = ''
    msg += f"As of now, the leading candidate for the shotgun is {gunner[2]} with {gunner[3]} projected points.\n"
    if gunner[0] == gunner[2]:
        msg +=f"{gunner[0]} currently has {gunner[1]} points. Yikes. :oof:\n"
    else: 
        msg +=f"Also keep an eye on {gunner[0]} who currently has {gunner[1]}.\n"
    return msg

def sendMessage(slack_client, msg):
  try:
    slack_client.chat_postMessage(
      channel=slack_channel,
      text=msg
    )
  except SlackApiError as e:
    logging.error('Request to Slack API Failed: {}.'.format(e.response.status_code))
    logging.error(e.response)
    
@app.route("/shotgun/test",methods=["POST"])
def command():
    if not verifier.is_valid_request(request.get_data(), request.headers):
        return make_response("invalid request", 403)
    info = request.form
    try:
        response = slack_client.chat_postMessage(
        channel='#{}'.format(info["channel_name"]), 
        text=commander.getMessage()
        )#.get()
    except SlackApiError as e:
        logging.error('Request to Slack API Failed: {}.'.format(e.response.status_code))
        logging.error(e.response)
        return make_response("", e.response.status_code)

    return make_response("", response.status_code)

 
def shotgunRun():
    league = League(league_id=leagueId,year=leagueYear,espn_s2=espnS2,swid=swid,debug=debugFlag)
    x = currentWeek()
    league.load_roster_week(week=x)
    endWeek = endOfWeek()
    gunner = shotgunCandidate(league,x)
    if endWeek:
        msg = endTheWeek(gunner[0],gunner[1],league,x)
        msg += talkShit(gunner[0])
    else:
        msg = midWeek(gunner)
        msg += talkShit(gunner[2])
        if gunner[0] == gunner[2]: pass
        else: msg += talkShit(gunner[0])
    #msg=""
    # sendMessage(slack_client,msg)
    return msg
        
        
        
        
        

#########

#########
if __name__ == '__main__':
    slack_client = WebClient(bot_token)
    verifier = SignatureVerifier(signing_secret)
    commander = Slash(shotgunRun())
    app.run()

